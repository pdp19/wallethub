package com.web.walletHub.testCases;

import java.text.ParseException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.web.walletHub.Pages.FacebookPage;
import com.web.walletHub.base.TestBase;

public class FirstAssignment extends TestBase{
	
	FacebookPage facebookPage;
	@BeforeMethod()
	public void setUp() throws ParseException {
		invokeBrowser(); //opening the browser
		//driver.manage().window().maximize(); // For windows
		driver.manage().window().fullscreen(); // For Mac it wors
		driver.manage().deleteAllCookies(); // deleting all cookies
		//opening the desired url
		driver.get("https://www.facebook.com/");
		facebookPage = new FacebookPage();
	}
	
	@Test()
	public void firstAssignment() {
		//Calling the desired method to post the status
		facebookPage.loginToFacebook("Provide@Your.Email", "andPasswordToTest","Hello World");
	}
	
	@AfterMethod()
	public void tearUp() {
		//closing the browser
		driver.close();
	}

}
