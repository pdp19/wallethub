package com.web.walletHub.util;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.web.walletHub.base.TestBase;

public class TestUtil extends TestBase{
	
	public static void waitForDisplay(WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	
	public static void scrollToElement(WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].scrollIntoView();", element);
		//js.executeScript("window.scrollTo(0,element.getLocation().y+)");
	}
	public static void mouseHover(WebElement element) {
		Actions builder = new Actions(driver);
		Action act = builder.moveToElement(element).build();
		act.perform();
	}
	public static void mouseHoverAndClick(WebElement element) {
		Actions builder = new Actions(driver);
		Action act = builder.moveToElement(element).click().build();
		act.perform();
	}

}
