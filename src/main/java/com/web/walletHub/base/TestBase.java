package com.web.walletHub.base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;


public class TestBase {

	public static WebDriver driver;	
	//public static Properties propDefault;
	
	//Creating a constructor which load the property file every time object of class has been created
	/*public TestBase() {
		propDefault = new Properties();
			try {
				FileInputStream ipDefault = new FileInputStream("/WalletHubHR/resources/default.properties");
				try {
					propDefault.load(ipDefault);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	//Getting the URL from properties file
	public static String getFirstURL() {
		String firstURL = propDefault.getProperty("First_URL");
		return firstURL;
		}
	//Getting the URL from properties file
	public static String getSecondURL() {
		String secondURL = propDefault.getProperty("Second_URL");
		return secondURL;
		}*/
	// Function to invoke the browser
	public void invokeBrowser() {
		//String browser = propDefault.getProperty("browser");
		//if(browser.equalsIgnoreCase("chrome")) {
			String chromePath = "/Users/lifesight/Desktop/WalletHubHR/resources/driver/chromedriver" ;
			System.setProperty("webdriver.chrome.driver", chromePath);
			
			//To disable notification pop up
			//Create a instance of ChromeOptions class
			ChromeOptions options = new ChromeOptions();
			//Add chrome switch to disable notification - "**--disable-notifications**"
			options.addArguments("--disable-notifications");
			driver = new ChromeDriver(options);
			
		//}
		// like this we can initiate the object of different webdriver classes
	}
}
