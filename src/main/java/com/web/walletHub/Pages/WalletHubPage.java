package com.web.walletHub.Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.web.walletHub.base.TestBase;
import com.web.walletHub.util.TestUtil;

public class WalletHubPage extends TestBase{
	
	//Storing web element as a repository 
		@FindBy(xpath="//input[contains(@placeholder,'Email')]")
		public static WebElement emailInputBox;
		
		@FindBy(xpath="//input[@placeholder='Password']")
		public static WebElement passInputBox;
		
		@FindBy(xpath="//input[@placeholder='Confirm Password']")
		public static WebElement confirmPassInputBox;
		
		///preceding-sibling::i
		@FindBy(xpath="//span[contains(.,'Get my free')]")
		public static WebElement chkBoxFreeCredit;
		
		@FindBy(xpath="//button[@type='button']")
		public static WebElement joinBtn;
		
		@FindBy(xpath="//h2[contains(.,'Thank you for registering')]")
		public static WebElement thankYouTxt;
		
		@FindBy(xpath="//span[contains(.,'Login')]")
		public static WebElement loginOption;
		
		@FindBy(xpath="//input[@placeholder = 'Email Address']")
		public static WebElement txtEmail;
		
		@FindBy(xpath="//input[@placeholder = 'Password']")
		public static WebElement txtPass;
		
		@FindBy(xpath="//button[contains(.,'Login')]")
		public static WebElement loginBtn;
		
		@FindBy(xpath="//a/span[contains(.,'Reviews')]")
		public static WebElement lnkReviews;
		
		@FindBy(xpath="//h3[contains(.,'Your Rating')]/following-sibling::review-star/div/*[name()='svg'][4]")
		public static WebElement ratingStar;
		
		@FindBy(xpath="//div[@class='dropdown second']")
		public static WebElement selectDD;
		
		@FindBy(xpath="//ul/li[contains(.,'Health Insurance')]")
		public static WebElement selectedValue;
		
		@FindBy(xpath="//div[@class='android']/textarea")
		public static WebElement txtAreaForReview;
		
		@FindBy(xpath="//div[@class='sub-nav-ct']/div[contains(.,'Submit')]")
		public static WebElement submitBtn;
		
		@FindBy(xpath="//li/a[contains(.,'Login')]")
		public static WebElement selectLoginOption;
		
		@FindBy(xpath="//span[contains(.,'Your Review')]/parent::div/parent::div/following-sibling::div[2]")
		public static WebElement updatedReviewTxt;
		
		//Initiating page factory by the constructor
		public WalletHubPage() {
			PageFactory.initElements(driver, this);
			//invokeBrowser();
		}
		
		// I thought we need to create account also by automation.
		public void createLightUser(String email, String pass) {
			TestUtil.waitForDisplay(emailInputBox);
			emailInputBox.sendKeys(email);
			passInputBox.sendKeys(pass);
			confirmPassInputBox.sendKeys(pass);
			chkBoxFreeCredit.click();
			joinBtn.click();
			TestUtil.waitForDisplay(thankYouTxt);
		}
		
		//Method to write review and veify the written review
		public void writeReviewWithoutLogin(String emailId, String password, String reviewText) throws InterruptedException {
			//After redirecting to the review page, it will wait for the review page to be open
			TestUtil.waitForDisplay(lnkReviews);
			//Cliking on review section
			lnkReviews.click();
			//Scrolling to the element for star
			TestUtil.scrollToElement(ratingStar);
			//Dynamic xpath, if user want to pass the number of star also we can do that by below xpath
			//WebElement starElement = driver.findElement(By.xpath("//h3[contains(.,'Your Rating')]/following-sibling::review-star/div/*[name()='svg']["+ numberOfStar +"]"));
			//Will hover the mouse on 4th star
			TestUtil.mouseHover(ratingStar);
			//Just for visibility i had given a wait so user see easily
			Thread.sleep(2000);
			//After hovering it will click on the 4th star
			TestUtil.mouseHoverAndClick(ratingStar);
			//wait for the droupdown to be present
			TestUtil.waitForDisplay(selectDD);
			//Clicking on dropdown and selecting the given value
			selectDD.click();
			selectedValue.click();
			//Writting the review given by user
			txtAreaForReview.sendKeys(reviewText);
			//Click on submit button
			submitBtn.click();
			//It will wait for login option to appear then login to the dummy account
			TestUtil.waitForDisplay(selectLoginOption);
			selectLoginOption.click();
			emailInputBox.sendKeys(emailId);
			passInputBox.sendKeys(password);
			joinBtn.click();
			//Wait for the review section and scroll to the review section
			TestUtil.waitForDisplay(updatedReviewTxt);
			TestUtil.scrollToElement(updatedReviewTxt);
			//Get the text of updated review
			String profileReviewedText = updatedReviewTxt.getText();
			//At last validating that review has been updated or not
			Assert.assertEquals(reviewText, profileReviewedText);
		}

}
