package com.web.walletHub.Pages;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.web.walletHub.base.TestBase;
import com.web.walletHub.util.TestUtil;

public class FacebookPage extends TestBase{
	
	//Storing web element as a repository 
	@FindBy(xpath="//input[@id='email']")
	public static WebElement userEmail;
	
	@FindBy(xpath="//input[@id='pass']")
	public static WebElement userPassword;
	
	@FindBy(xpath="//input[@type='submit']")
	public static WebElement loginBtn;
	
	@FindBy(xpath="//div/textarea[contains(@title,'Write something here...')]")
	public static WebElement inputBoxToWrite;
	
	@FindBy(xpath="//div[@class='_45wg _69yt']/button[contains(.,'Post')]")
	public static WebElement postbtn;
	
	public FacebookPage() {
		PageFactory.initElements(driver, this);
		//invokeBrowser();
	}
	//Function to login and to post into Facebook
	public void loginToFacebook(String email, String password,String post) {
		//loging into facebook
		TestUtil.waitForDisplay(userEmail);
		userEmail.sendKeys(email);
		userPassword.sendKeys(password);
		loginBtn.click();
		TestUtil.waitForDisplay(inputBoxToWrite);
		//WebDriverWait wait = new WebDriverWait(driver, 20);
        //wait.until(ExpectedConditions.alertIsPresent());
		//Alert alert = driver.switchTo().alert();
		//alert.dismiss();
		//driver.switchTo().alert().dismiss();
		//Posting into facebook
		inputBoxToWrite.sendKeys(post);
		TestUtil.waitForDisplay(postbtn);
		postbtn.click();
	}
}
